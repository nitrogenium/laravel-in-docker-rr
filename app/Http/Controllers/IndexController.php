<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Cache;

class IndexController extends Controller
{
    public function index()
    {

        $count = Cache::increment('count');

        return view('index', [
            'users' => User::all(),
            'count' => $count,
        ]);

    }

}
